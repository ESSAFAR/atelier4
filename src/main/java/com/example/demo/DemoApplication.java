package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	private String unusedVariable = "This is not used";

	@GetMapping("/")
	public String home() {
		return "Spring is here!";
	}

	@GetMapping("/coucou")
    public String sayHello() {
        return "coucou";
    }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


}


